const { Pool } = require('pg');
 
class PlaylistsService {
  constructor() {
    this._pool = new Pool();
  }
 
  async getPlaylists(playlistId) {
    const query2 = {
      text: `SELECT playlists.* FROM playlists
      WHERE playlists.id = $1`,
      values: [playlistId],
    };
    const query = {
      text: `SELECT playlistsongs.song_id as id, songs.title as title, songs.performer 
      FROM playlistsongs JOIN songs ON playlistsongs.song_id = songs.id WHERE playlist_Id = $1`,
      values: [playlistId],
    }
    const songs = await this._pool.query(query);
    const playlist = await this._pool.query(query2);

    const result = {
      playlistId: playlist.rows[0].id,
      name: playlist.rows[0].name,
      songs: songs.rows
    }
    return result;
  }
}
 
module.exports = PlaylistsService;

